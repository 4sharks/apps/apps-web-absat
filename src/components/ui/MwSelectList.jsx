import React from 'react'

const MwSelectList = (props) => {
    const {label} = props
    return (
        <>
            <label className='text-sm text-slate-500 pt-1' htmlFor="Customer">{label}</label>
            <select
                className=' text-slate-600 border rounded border-0 pt-2 pb-3 text-center' 
            >
                <option value="">Unit</option>
            </select>
        </>
    )
}

export default MwSelectList